import { Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'app-hijo',
  templateUrl: './hijo.component.html',
  styleUrls: ['./hijo.component.css']
})
export class HijoComponent implements OnInit {

  @Input('master') variable:string;
  @Input('obj') objeto:any;

  constructor() {

  }

  ngOnInit() { }

  saludar(){
    console.log(this.variable);
    console.log(this.objeto);


  }

}
