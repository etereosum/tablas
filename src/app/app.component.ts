import { Component, Input} from '@angular/core';
import { HijoComponent } from './hijo/hijo.component';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  master = 'Master';
  obj ={
    'nombre' : 'Pablo',
    'edad'   :'36'
  }


}
